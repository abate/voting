#!/bin/bash

start() {
  local nodes=$1
  docker network create --internal votingnet

  for i in $nodes; do
    COMPOSE_PROJECT_NAME=$i docker-compose up -d
  done
}

stop() {
  local nodes=$1
  for i in $nodes; do
    COMPOSE_PROJECT_NAME=$i docker-compose down
  done
}

clean() {
  local nodes=$1
  for i in $nodes; do
    COMPOSE_PROJECT_NAME=$i docker-compose rm -f
  done
  docker network rm votingnet
}

logs() {
  COMPOSE_PROJECT_NAME=1 docker-compose logs -f --tail=50 node
}

usage() {
  DESCRIPTION=
  cat << EOF
usage: $0 options

$DESCRIPTION

OPTIONS:
   -h      Show this message
   -v      Verbose
   -d      Debug
EOF
}

VERBOSE=
DEBUG=
CMDARG=
NODES=$(seq 2)

while getopts "vhdn:" flag
do
  case "$flag" in
    n) NODES=$(seq "$OPTARG") ;;
    d) set -x ; DEBUG=true;;
    v) VERBOSE=true ;;
    h) usage ; exit 0 ;;
    *) break ;;
  esac
done

shift $((OPTIND-1))
CMDARG="$@"

case "$CMDARG" in
  start) start "$NODES" ;;
  stop) stop "$NODES" ;;
  clean) clean "$NODES" ;;
  logs) logs ;;
esac

exit 0
